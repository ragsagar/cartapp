const getPageLimitAndOffset = (pageValue, limitValue) => {
  const page = parseInt(pageValue, 10) || 1;
  const limit = parseInt(limitValue, 10) || 10;
  const offset = (page - 1) * limit;
  return [page, limit, offset];
};

module.exports = {
  getPageLimitAndOffset,
};
