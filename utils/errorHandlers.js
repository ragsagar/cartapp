const catchAsyncError = (func) => (req, res, next) => {
  return Promise.resolve(func(req, res, next)).catch(next);
};

let constructValidationErrorMessage = (fieldValue, messageValue) => {
  return {
    errors: [{ field: fieldValue, message: messageValue }],
    message: "Validation error",
  };
};

module.exports = {
  catchAsyncError,
  constructValidationErrorMessage,
};
