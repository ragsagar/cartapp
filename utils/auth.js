const jwt = require("jsonwebtoken");

const generateJWTToken = (payload) => {
  return jwt.sign(payload, process.env.JWT_TOKEN_SECRET);
};

module.exports = {
    generateJWTToken,
}