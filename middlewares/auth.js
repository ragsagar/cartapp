const { User } = require("../models");
const jwt = require("jsonwebtoken");
const { StatusCodes } = require("http-status-codes");

const authenticateToken = async (req, res, next) => {
  const authHeader = req.headers["authorization"];
  const token = authHeader && authHeader.split(" ")[1];
  if (!token)
    return res
      .status(StatusCodes.UNAUTHORIZED)
      .json({ error: "No token provided" });
  jwt.verify(token, process.env.JWT_TOKEN_SECRET, async (err, payload) => {
    if (err)
      return res.status(StatusCodes.FORBIDDEN).json({ error: "Invalid token" });
    const user = await User.findOne({ where: { id: payload.id } });
    if (!user) {
      return res.status(StatusCodes.FORBIDDEN).json({ error: "Invalid token" })
    }
    req.user = user;
    next();
  });
};

module.exports = {
  authenticateToken,
};
