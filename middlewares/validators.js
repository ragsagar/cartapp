const { StatusCodes } = require("http-status-codes");

const validateBody = (schema) => (req, res, next) => {
  const result = schema.validate(req.body, { abortEarly: false });
  if (result.error) {
    const errors = result.error.details.map((item) => ({
      field: item.context.key,
      message: item.message,
    }));
    const errorResponse = { errors: errors, message: "Validation error" };
    return res.status(StatusCodes.BAD_REQUEST).json(errorResponse);
  }
  next();
};

module.exports = {
  validateBody,
};
