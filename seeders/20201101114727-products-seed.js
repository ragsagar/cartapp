"use strict";

let getDateAfterDays = (days) => {
  const now = new Date();
  now.setDate(now.getDate() + days);
  return now;
};

let getRandomInt = (max) => {
  return Math.floor(Math.random() * Math.floor(max));
};

module.exports = {
  up: async (queryInterface) => {
    const createdDate = new Date();
    await queryInterface.bulkInsert(
      "Categories",
      [
        {
          name: "Mobile",
          slug: "mobile",
          createdAt: createdDate,
          updatedAt: createdDate,
        },
        {
          name: "Books",
          slug: "books",
          createdAt: createdDate,
          updatedAt: createdDate,
        },
      ],
      {}
    );
    const mobile = await queryInterface.sequelize.query(
      `SELECT id FROM Categories WHERE slug = "mobile" LIMIT 1;`
    );
    const books = await queryInterface.sequelize.query(
      `SELECT id FROM Categories WHERE slug = "books" LIMIT 1;`
    );
    const mobileId = mobile[0][0].id;
    const bookId = books[0][0].id;
    await queryInterface.bulkInsert(
      "Products",
      [
        {
          title: "Iphone 12",
          slug: "iphone-12",
          sku: "MB1001",
          stockLevel: getRandomInt(20),
          expiresAt: getDateAfterDays(getRandomInt(10)),
          price: 3500,
          createdAt: createdDate,
          updatedAt: createdDate,
          CategoryId: mobileId,
        },
        {
          title: "Samsung Note 20",
          slug: "samsung-note-20",
          sku: "MB1002",
          stockLevel: getRandomInt(20),
          expiresAt: getDateAfterDays(getRandomInt(10)),
          price: 3000,
          createdAt: createdDate,
          updatedAt: createdDate,
          CategoryId: mobileId,
        },
        {
          title: "Huawei P40 Pro",
          slug: "huawei-p40-pro",
          sku: "MB1003",
          stockLevel: getRandomInt(20),
          expiresAt: getDateAfterDays(getRandomInt(10)),
          price: 2000,
          createdAt: createdDate,
          updatedAt: createdDate,
          CategoryId: mobileId,
        },
        {
          title: "OnePlus 8 Pro",
          slug: "oneplus-8-pro",
          sku: "MB1004",
          stockLevel: getRandomInt(20),
          expiresAt: getDateAfterDays(getRandomInt(10)),
          price: getRandomInt(1000),
          createdAt: createdDate,
          updatedAt: createdDate,
          CategoryId: mobileId,
        },
        {
          title: "Xiaomi Mi 10",
          slug: "xiaomi-mi-10",
          sku: "MB1005",
          stockLevel: getRandomInt(20),
          expiresAt: getDateAfterDays(getRandomInt(10)),
          price: getRandomInt(1000),
          createdAt: createdDate,
          updatedAt: createdDate,
          CategoryId: mobileId,
        },
        {
          title: "Motor Razr",
          slug: "motor-razr",
          sku: "MB1006",
          stockLevel: getRandomInt(20),
          expiresAt: getDateAfterDays(getRandomInt(10)),
          price: getRandomInt(1000),
          createdAt: createdDate,
          updatedAt: createdDate,
          CategoryId: mobileId,
        },
        {
          title: "Moto G9 Play",
          slug: "motor-g9-play",
          sku: "MB1007",
          stockLevel: getRandomInt(20),
          expiresAt: getDateAfterDays(getRandomInt(10)),
          price: getRandomInt(1000),
          createdAt: createdDate,
          updatedAt: createdDate,
          CategoryId: mobileId,
        },
        {
          title: "Huawei Nova 7",
          slug: "huawei-nova-7",
          sku: "MB1008",
          stockLevel: getRandomInt(20),
          expiresAt: getDateAfterDays(getRandomInt(10)),
          price: getRandomInt(1000),
          createdAt: createdDate,
          updatedAt: createdDate,
          CategoryId: mobileId,
        },
        {
          title: "Harry Potter",
          slug: "harry-potter",
          sku: "BK1001",
          stockLevel: getRandomInt(20),
          expiresAt: getDateAfterDays(getRandomInt(10)),
          price: getRandomInt(1000),
          createdAt: createdDate,
          updatedAt: createdDate,
          CategoryId: bookId,
        },
        {
          title: "Godfather",
          slug: "godfather",
          sku: "BK1002",
          stockLevel: getRandomInt(20),
          expiresAt: getDateAfterDays(getRandomInt(10)),
          price: getRandomInt(1000),
          createdAt: createdDate,
          updatedAt: createdDate,
          CategoryId: bookId,
        },
        {
          title: "Alchemist",
          slug: "alchemist",
          sku: "BK1003",
          stockLevel: getRandomInt(20),
          expiresAt: getDateAfterDays(getRandomInt(10)),
          price: getRandomInt(1000),
          createdAt: createdDate,
          updatedAt: createdDate,
          CategoryId: bookId,
        },
      ],
      {}
    );
  },

  down: async (queryInterface) => {
    await queryInterface.bulkDelete("Products", null, {});
    await queryInterface.bulkDelete("Categories", null, {});
  },
};
