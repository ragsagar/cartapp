Cartapp
=======

#### Setting up and running the project locally

```
$ git clone https://gitlab.com/ragsagar/cartapp.git
$ cd cartapp
$ docker-compose up
$ curl http://localhost:3000/api/v1/products/
```

Test database is not available as part of docker compose. it needs mysql to be setup separately and run separately from host.

```
$ mv .env.test.example .env
$ npm run test
```

#### Data model

![ER Diagram](diagrams/er.svg)

##### `Carts` and `CartItems`

These tables are used for storing the cart of user and items in it. There will be only one cart with `status` as `new` for a user. Once checkout is done, `status` of the cart will be changed to `complete`. Next time user adds item to cart, New cart will be created for the user with `status` as `new`. 

For implementing checkout, An `Orders` table and `OrderItems` table can be added. Carts and CarItems data can be copied to it on checkout. Another option is that we can replace the `Carts` table and `CartItems` table with `Orders` and `OrderItems` tables. Then we can manage the state of Orders table as cart and order using a state machine.

##### `Products` and `Categories`

These tables are for storing products and its related categories. Slug in product and category has unique index, So the query using slug in product list and product detail API (not available) should be using these indices.

A `Variants` table can be introduced to keep different shapes or sizes of products. In that case, `CartItems` will have relation to `Variants` table instead of `Products`. For example "One plus 8" will be a single product, its various colours like "One plus 8 Red" and "One plus 8 Blue" will be Variants. When we add items in cart, we will be adding this variant instead of product. This is not done now to keep it simple.

##### `Users`
Table for storing users. `email` column has unique index. So queries using it should be faster. Password is hashed and stored.

#### APIs

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/3296ec30f7f8dc667cea)

This app contains three set of APIs. 

- Authentication APIs
    - [API for user login.](#login-api)
    - [API for user registartion.](#registration-api)
- Cart APIs
    - [API for adding new product to cart.](#add-product-to-cart-api)
    - [API for fetching cart data and it's items.](#fetch-cart-api)
    - [API for update a particular cart item.](#cart-update-api)
    - [API for deleting a particular cart item.](#delete-cart-item-api)
- Product API
    - [API for fetching all products.](#product-list-api)

[Postman Documentation](https://documenter.getpostman.com/view/7649609/TVYM4bUt)

##### Registration API

API for creating user in database.

Request:
```
POST /api/v1/auth/register/
Body:
{
    "email": "user@gmail.com",
    "password": "verystrongpassword",
    "firstName": "Rust",
    "lastName": "Cohle"
}
```
Response:
```
Status Code: 200
Body:
{
    "id": 3,
    "firstName": "Rust",
    "lastName": "Cohle",
    "email": "user@gmail.com"
}
```

##### Login API

API to login and get JWT token.

Request:

```
POST /api/v1/auth/login/

Body:
{
    "email": "user@gmail.com",
    "password": "verystrongpassword"
}
```

Response:
```
{
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwiaWF0IjoxNjA0MjYwNDE0fQ.VD-ts0re3fDh3U-yDjW7qcnVsR81tnNcCFCleQOBlE0"
}
```
All cart APIs require JWT token for authentication.

##### Product List API

Open API to fetch list of all products. This can be used to show the catalogue of products. This API can be cached using redis or memcache for faster response. Also, it is better to sync all products to elasticsearch or algolia and show catalogue using it instead of querying database. Algolia and elasticsearch provides faster search. 

Request:

```
GET /api/v1/products/
```

Response:

```
{
    "data": [
        {
            "id": 1,
            "title": "Iphone 12",
            "slug": "iphone-12",
            "sku": "MB1001",
            "price": "3500",
            "stockLevel": 12
        },
        {
            "id": 2,
            "title": "Samsung Note 20",
            "slug": "samsung-note-20",
            "sku": "MB1002",
            "price": "3000",
            "stockLevel": 18
        },
        {
            "id": 3,
            "title": "Huawei P40 Pro",
            "slug": "huawei-p40-pro",
            "sku": "MB1003",
            "price": "2000",
            "stockLevel": 19
        }
    ],
    "totalCount": 11,
    "page": 1,
    "limit": 3
}
```

For pagination, this API accepts two query params, `page` and `limit`.

Request:
```
GET /api/v1/products/?page=2&limit=1
```
Response:
```
{
    "data": [
        {
            "id": 2,
            "title": "Samsung Note 20",
            "slug": "samsung-note-20",
            "sku": "MB1002",
            "price": "3000",
            "stockLevel": 18
        }
    ],
    "totalCount": 11,
    "page": 2,
    "limit": 1
}
```

For filtering by category, this API accepts a query param `category`. Category slug should be given as input to this param.

Request:
```
GET /api/v1/products/?category=books
```
Response:
```
{
    "data": [
        {
            "id": 9,
            "title": "Harry Potter",
            "slug": "harry-potter",
            "sku": "BK1001",
            "price": "108",
            "stockLevel": 2
        },
        {
            "id": 10,
            "title": "Godfather",
            "slug": "godfather",
            "sku": "BK1002",
            "price": "628",
            "stockLevel": 8
        }
    ],
    "totalCount": 3,
    "page": 1,
    "limit": 2
}
```

##### Add product to cart API

API to add product to cart. This can be used to add item to cart from the product detail page. `productId` is the id of the product received in the product list API. Returns 201, with cart item and quantity on success.

Request:
```
POST /api/v1/carts/mine/items/
Headers:
Authorization: Bearer <jwt-token>
Body:
{
    "productId": 11,
    "quantity": 1
}
```
Response:
```
Status Code: 201
Body:
{
    "id": 4,
    "sku": "BK1003",
    "price": "70",
    "discount": "0",
    "quantity": 1,
    "CartId": 2,
    "ProductId": 11,
    "createdAt": "2020-11-01T19:47:35.000Z",
    "updatedAt": "2020-11-01T19:47:35.000Z"
}
```

##### Fetch cart API

API to get cart. This can be used to show all cart items and its total in cart detail page. This API can be can be cached for better response time.

Request:
```
GET /api/v1/carts/mine/
Headers:
Authorization: Bearer <jwt-token>
```

Response
```
Status Code: 200
{
    "id": 2,
    "subTotal": 924,
    "discount": 0,
    "shippingCharge": 0,
    "total": 924,
    "items": [
        {
            "id": 3,
            "title": "OnePlus 8 Pro",
            "price": "784",
            "quantity": 1,
            "stockLevel": 6,
            "productId": 4,
            "sku": "MB1004",
            "discount": "0"
        },
        {
            "id": 4,
            "title": "Alchemist",
            "price": "70",
            "quantity": 2,
            "stockLevel": 18,
            "productId": 11,
            "sku": "BK1003",
            "discount": "0"
        }
    ]
}
```

Empty JSON will be returned with 200 status code if there are no items in the cart.


##### Cart update API

API to update quantity of a cart item. This can be used to change the quantity of a cart item from cart page.
Id in the url is the cart item id

Request:
```
PUT /api/v1/carts/mine/items/<cartItemId>/
Headers:
Authorization: Bearer <jwt-token>
Body:
{
    "quantity": 2
}
```
Response:

```
Status Code: 200
Body:
{
    "id": <cartItemId>,
    "sku": "BK1003",
    "price": "70",
    "discount": "0",
    "quantity": 2,
    "CartId": 2,
    "ProductId": 11,
    "createdAt": "2020-11-01T19:47:35.000Z",
    "updatedAt": "2020-11-01T19:57:26.000Z",
    "Cart": {
        "id": 2,
        "status": "new",
        "UserId": 2,
        "createdAt": "2020-11-01T19:46:26.000Z",
        "updatedAt": "2020-11-01T19:46:26.000Z"
    },
    "Product": {
        "id": 11,
        "title": "Alchemist",
        "slug": "alchemist",
        "sku": "BK1003",
        "stockLevel": 18,
        "expiresAt": "2020-11-09T19:36:46.000Z",
        "price": "70",
        "CategoryId": 2,
        "createdAt": "2020-11-01T19:36:46.000Z",
        "updatedAt": "2020-11-01T19:36:46.000Z"
    }
}
```

##### Delete cart item API
API to delete a cart item. This can be used to remove an item from cart from the cart page. Returns 204 on succesful delete.

Request:
```
DELETE /api/v1/carts/mine/items/<cartItemId>/
Headers:
Authorization: Bearer <jwt-token>
```
Response:
```
Status Code: 204
```
