const Joi = require("joi");

const quantityValidator = Joi.number().integer().min(1).max(20).required();

const addCartItemSchema = Joi.object({
  productId: Joi.number().integer().min(1).required(),
  quantity: quantityValidator,
});

const updateCartItemSchema = Joi.object({
  quantity: quantityValidator,
})

module.exports = {
  addCartItemSchema,
  updateCartItemSchema
};
