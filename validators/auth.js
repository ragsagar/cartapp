const Joi = require("joi");

const emailValidator = Joi.string().email().min(4).max(50).required();
const passwordValidator = Joi.string().min(6).max(150).required();
const nameValidator = Joi.string().required();

const registerUserSchema = Joi.object().keys({
  firstName: nameValidator,
  lastName: nameValidator,
  email: emailValidator,
  password: passwordValidator,
});

const loginUserSchema = Joi.object().keys({
  email: emailValidator,
  password: passwordValidator,
});

module.exports = {
  registerUserSchema,
  loginUserSchema,
};
