const express = require("express");
const router = express.Router();

const {
  getCart,
  addItemToCart,
  deleteItem,
  updateCartItem,
} = require("../controllers/cart.controller");
const { authenticateToken } = require("../middlewares/auth");
const { validateBody } = require("../middlewares/validators");
const {
  addCartItemSchema,
  updateCartItemSchema,
} = require("../validators/cart");

router.get("/mine", authenticateToken, getCart);
router.post(
  "/mine/items",
  authenticateToken,
  validateBody(addCartItemSchema),
  addItemToCart
);
router.delete("/mine/items/:id", authenticateToken, deleteItem);
router.put(
  "/mine/items/:id",
  authenticateToken,
  validateBody(updateCartItemSchema),
  updateCartItem
);

module.exports = router;
