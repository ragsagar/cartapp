const express = require("express");
const { registerUser, loginUser } = require("../controllers/auth.controller");
const { validateBody } = require("../middlewares/validators");
const { registerUserSchema, loginUserSchema } = require("../validators/auth");

const router = express.Router();

router.post("/register", validateBody(registerUserSchema), registerUser);
router.post("/login", validateBody(loginUserSchema), loginUser);

module.exports = router;
