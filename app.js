var express = require("express");
var path = require("path");
var logger = require("morgan");

const productsRouter = require("./routes/products");
const authRouter = require("./routes/auth");
const cartRouter = require("./routes/cart")

const app = express();

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, "public")));

app.use("/api/v1/products", productsRouter);
app.use("/api/v1/auth", authRouter);
app.use("/api/v1/carts", cartRouter)

app.use((err, req, res, next) => { // eslint-disable-line no-unused-vars
  const message = "Internal server error"
  res.status(err.status || 500).json({error: err.message || message});
})

module.exports = app;
