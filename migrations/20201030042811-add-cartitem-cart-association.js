"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.addColumn("CartItems", "CartId", {
      type: Sequelize.INTEGER,
      references: {
        model: "Carts",
        key: "id",
      },
      onUpdate: "CASCADE",
      onDelete: "CASCADE",
    });
  },

  down: async (queryInterface) => {
    return queryInterface.removeColumn("CartItems", "CartId");
  },
};
