const httpMocks = require("node-mocks-http");
const { StatusCodes } = require("http-status-codes");

const { User } = require("../../models");
const AuthController = require("../../controllers/auth.controller");

User.create = jest.fn();
User.findOne = jest.fn();

let req, res, next;

beforeEach(() => {
  req = httpMocks.createRequest();
  res = httpMocks.createResponse();
  next = jest.fn();
});

describe("AuthController.registerUser", () => {
  it("should have a registerUser function", () => {
    expect(typeof AuthController.registerUser).toBe("function");
  });
  it("should call User.create", async () => {
    const expectedArgs = { firstName: "Rust", lastName: "Cohle" };
    req.body = expectedArgs;
    await AuthController.registerUser(req, res, next);
    expect(User.create).toBeCalledWith(expectedArgs);
  });
  it("should return 201 status code and json", async () => {
    const expectedData = { firstName: "Rust", lastName: "Cohle" };
    User.create.mockReturnValue(expectedData);
    await AuthController.registerUser(req, res, next);
    expect(res.statusCode).toBe(StatusCodes.CREATED);
    expect(res._isEndCalled()).toBeTruthy();
    expect(res._getJSONData()).toStrictEqual(expectedData);
  });
  it("should handle error", async () => {

    const expectedArg = { error: "Something went wrong" };
    const rejectedPromise = Promise.reject(expectedArg);
    User.create.mockReturnValue(rejectedPromise);
    await AuthController.registerUser(req, res, next);
    expect(next).toBeCalledWith(expectedArg);
  });
});

describe("AuthController.loginUser", () => {
  it("should have a loginUser function", () => {
    expect(typeof AuthController.loginUser).toBe("function");
  });
  it("should call User.findOne", async () => {
    const expectedArgs = { where: { email: "rust@gmail.com" } };
    req.body = { email: "rust@gmail.com", password: "password" };
    await AuthController.loginUser(req, res, next);
    expect(User.findOne).toBeCalledWith(expectedArgs);
  });
  it ("should return 401 when password doesn't match", async () => {
    User.findOne.mockReturnValue({
      id: 1,
      email: "rust@gmail.com",
      generateToken: () => "dummytoken",
      comparePassword: async () => Promise.resolve(false),
    });
    await AuthController.loginUser(req, res, next);
    expect(res._isEndCalled()).toBeTruthy();
    expect(res.statusCode).toBe(StatusCodes.UNAUTHORIZED);
    expect(res._getJSONData()).toStrictEqual({message: "Invalid username or password"});
  })
  it ("should return 401 when user doesn't exist", async () => {
    User.findOne.mockReturnValue(null);
    await AuthController.loginUser(req, res, next);
    expect(res._isEndCalled()).toBeTruthy();
    expect(res.statusCode).toBe(StatusCodes.UNAUTHORIZED);
    expect(res._getJSONData()).toStrictEqual({message: "Invalid username or password"});
  })
  it("should return 200 status code and token", async () => {
    User.findOne.mockReturnValue({
      id: 1,
      email: "rust@gmail.com",
      generateToken: () => "dummytoken",
      comparePassword: async () => Promise.resolve(true),
    });
    await AuthController.loginUser(req, res, next);
    expect(res._isEndCalled()).toBeTruthy();
    expect(res.statusCode).toBe(StatusCodes.OK);
    expect(res._getJSONData().token).toBe("dummytoken");
  });
  it("should handle promise rejection error", async () => {
    const expectedArg = { error: "Something went wrong" };
    const rejectedPromise = Promise.reject(expectedArg);
    User.findOne.mockReturnValue(rejectedPromise);
    await AuthController.loginUser(req, res, next);
    expect(next).toBeCalledWith(expectedArg);
  });
});
