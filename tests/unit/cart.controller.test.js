const { StatusCodes } = require("http-status-codes");
const httpMocks = require("node-mocks-http");

const CartController = require("../../controllers/cart.controller.js");
const models = require("../../models");
const {
  mockCart,
  mockCartItem,
  mockProduct,
  mockCartResponse,
} = require("../mock-data/cart");

let req, res, next;

beforeEach(() => {
  req = httpMocks.createRequest();
  res = httpMocks.createResponse();
  next = jest.fn();
  models.Cart.findOne = jest.fn();
  models.Cart.findOrCreate = jest.fn();
  models.CartItem.findOrCreate = jest.fn();
  models.CartItem.findOne = jest.fn();
  models.Product.findByPk = jest.fn();
  models.CartItem.destroy = jest.fn();
});

describe("CartController.getCart", () => {
  beforeEach(() => {
    req.user = { id: 1 };
    models.Cart.findOne.mockReturnValue(mockCart);
  });

  it("should have a getCartItems function", () => {
    expect(typeof CartController.getCart).toBe("function");
  });

  it("should call Cart.findOne", async () => {
    await CartController.getCart(req, res, next);
    expect(models.Cart.findOne).toBeCalledWith({
      where: { UserId: 1, status: "new" },
      include: { model: models.CartItem, include: { model: models.Product } },
    });
  });

  it("should return 200 response code", async () => {
    await CartController.getCart(req, res, next);
    expect(res.statusCode).toBe(StatusCodes.OK);
  });

  it("should return cart items as json", async () => {
    await CartController.getCart(req, res, next);
    expect(res._getJSONData()).toStrictEqual(mockCartResponse);
  });

  it("should return empty cart response when there is no active cart", async () => {
    models.Cart.findOne.mockReturnValue(null);
    await CartController.getCart(req, res, next);
    expect(res.statusCode).toBe(StatusCodes.OK);
    expect(res._getJSONData()).toStrictEqual({});
  });

  it("should handle promise rejection error", async () => {
    const expectedArg = { error: "Something went wrong" };
    const rejectedPromise = Promise.reject(expectedArg);
    models.Cart.findOne.mockReturnValue(rejectedPromise);
    await CartController.getCart(req, res, next);
    expect(next).toBeCalledWith(expectedArg);
  });
});

describe("CartController.addItemToCart", () => {
  beforeEach(() => {
    req.user = { id: 1 };
    req.body = { productId: 3, quantity: 2 };
    models.Product.findByPk.mockReturnValue(mockProduct);
    models.Cart.findOrCreate.mockReturnValue([mockCart, true]);
    models.CartItem.findOrCreate.mockReturnValue([mockCartItem, true]);
  });

  it("should have addItemToCart function", async () => {
    expect(typeof CartController.addItemToCart).toBe("function");
  });

  it("should call Cart.findOrCreate", async () => {
    await CartController.addItemToCart(req, res, next);
    expect(models.Cart.findOrCreate).toBeCalledWith({
      where: { UserId: 1, status: "new" },
    });
  });

  it("should call CartItem.findOrCreate", async () => {
    await CartController.addItemToCart(req, res, next);
    expect(models.CartItem.findOrCreate).toBeCalledWith({
      where: { CartId: 10, ProductId: 3 },
      defaults: { quantity: 2 },
    });
  });

  it("should return 201 when new cart item is added", async () => {
    await CartController.addItemToCart(req, res, next);
    expect(res.statusCode).toBe(StatusCodes.CREATED);
  });

  it("should atomically increment the quantity if cart item already exists", async () => {
    const mockItem = {
      increment: jest.fn(),
      quantity: 1,
      Product: { stockLevel: 4 },
    };
    models.CartItem.findOrCreate.mockReturnValue([mockItem, false]);
    await CartController.addItemToCart(req, res, next);
    expect(mockItem.increment).toBeCalledWith("quantity", { by: 2 });
  });

  it("should return bad request if the given product doesn't exist", async () => {
    models.Product.findByPk.mockReturnValue(null);
    const errorMessage = {
      errors: [{ field: "productId", message: "Invalid productId" }],
      message: "Validation error",
    };
    await CartController.addItemToCart(req, res, next);
    expect(res.statusCode).toBe(StatusCodes.BAD_REQUEST);
    expect(res._getJSONData()).toStrictEqual(errorMessage);
  });

  it("should return bad request if product has not enough stockLevel and not in cart", async () => {
    models.Product.findByPk.mockReturnValue({ stockLevel: 1 });
    const errorMessage = {
      errors: [{ field: "quantity", message: "Not enough items in stock" }],
      message: "Validation error",
    };
    await CartController.addItemToCart(req, res, next);
    expect(res.statusCode).toBe(StatusCodes.BAD_REQUEST);
    expect(res._getJSONData()).toStrictEqual(errorMessage);
  });

  it("should return bad request if product has not enough stockLevel and already present in cart", async () => {
    const mockItem = {
      increment: jest.fn(),
      quantity: 1,
    };
    const errorMessage = {
      errors: [{ field: "quantity", message: "Not enough items in stock" }],
      message: "Validation error",
    };
    models.Product.findByPk.mockReturnValue({ stockLevel: 2 });
    models.CartItem.findOrCreate.mockReturnValue([mockItem, false]);
    await CartController.addItemToCart(req, res, next);
    expect(res.statusCode).toBe(StatusCodes.BAD_REQUEST);
    expect(res._getJSONData()).toStrictEqual(errorMessage);
  });

  it("should handle promise rejection error", async () => {
    const expectedArg = { error: "Something went wrong" };
    const rejectedPromise = Promise.reject(expectedArg);
    models.Product.findByPk.mockReturnValue(rejectedPromise);
    await CartController.addItemToCart(req, res, next);
    expect(next).toBeCalledWith(expectedArg);
  });
});

describe("CartController.deleteItem", () => {
  let cartItem;
  beforeEach(() => {
    req.user = { id: 1 };
    req.params = { id: 2 };
    models.Cart.findOne.mockReturnValue(mockCart);
    cartItem = { destroy: jest.fn() };
    models.CartItem.findOne.mockReturnValue(cartItem);
  });

  it("should have deleteItem function", () => {
    expect(typeof CartController.deleteItem).toBe("function");
  });

  it("should call CartItem.findOne", async () => {
    await CartController.deleteItem(req, res, next);
    expect(models.CartItem.findOne).toBeCalled();
  });

  it("should call cartItem.destroy", async () => {
    await CartController.deleteItem(req, res, next);
    expect(cartItem.destroy).toBeCalled();
  });

  it("should return 204 on successfull delete", async () => {
    await CartController.deleteItem(req, res, next);
    expect(res.statusCode).toBe(StatusCodes.NO_CONTENT);
  });

  it("should return 404 if given item doesnt exist", async () => {
    models.CartItem.findOne.mockReturnValue(null);
    await CartController.deleteItem(req, res, next);
    expect(res.statusCode).toBe(StatusCodes.NOT_FOUND);
    expect(res._getJSONData()).toStrictEqual({
      message: "Cart item not found",
    });
  });

  it("should handle promise rejection error", async () => {
    const expectedArg = { error: "Something went wrong" };
    const rejectedPromise = Promise.reject(expectedArg);
    models.CartItem.findOne.mockReturnValue(rejectedPromise);
    await CartController.deleteItem(req, res, next);
    expect(next).toBeCalledWith(expectedArg);
  });
});

describe("CartController.updateCartItem", () => {
  beforeEach(() => {
    req.user = { id: 1 };
    req.params = { id: 9 };
    req.body = { quantity: 2 };
  });

  it("should have CartController.updateCartItem", () => {
    expect(typeof CartController.updateCartItem).toBe("function");
  });

  it("should call CartItem.findOne", async () => {
    await CartController.updateCartItem(req, res, next);
    expect(models.CartItem.findOne).toBeCalled();
  });

  it("should return bad request if there is no active cart or caritem", async () => {
    models.CartItem.findOne.mockReturnValue(null);
    await CartController.updateCartItem(req, res, next);
    expect(res.statusCode).toBe(StatusCodes.NOT_FOUND);
    expect(res._getJSONData()).toStrictEqual({
      message: "Cart item not found",
    });
  });

  it("should call update, return 200 when there is cartitem", async () => {
    const cartItem = {
      update: jest.fn(),
      reload: jest.fn(),
      Product: mockProduct,
    };
    models.CartItem.findOne.mockReturnValue(cartItem);
    await CartController.updateCartItem(req, res, next);
    expect(cartItem.update).toBeCalledWith({ quantity: 2 });
    expect(res.statusCode).toBe(StatusCodes.OK);
  });

  it("should return validation error when quantity greater than stockLevel", async () => {
    req.body = { quantity: 11 };
    const cartItem = {
      update: jest.fn(),
      reload: jest.fn(),
      Product: mockProduct,
    };
    models.CartItem.findOne.mockReturnValue(cartItem);
    await CartController.updateCartItem(req, res, next);
    expect(res.statusCode).toBe(StatusCodes.BAD_REQUEST);
    expect(res._getJSONData()).toStrictEqual({
      errors: [{ field: "quantity", message: "Not enough items in stock" }],
      message: "Validation error",
    });
  });

  it("should handle promise rejection error", async () => {
    const expectedArg = { error: "Something went wrong" };
    const rejectedPromise = Promise.reject(expectedArg);
    models.CartItem.findOne.mockReturnValue(rejectedPromise);
    await CartController.updateCartItem(req, res, next);
    expect(next).toBeCalledWith(expectedArg);
  });
});
