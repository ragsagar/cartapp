const httpMocks = require("node-mocks-http");

const ProductController = require("../../controllers/products.controller");
const models = require("../../models");
const allProductsData = require("../mock-data/all-products.json");

models.Product.findAndCountAll = jest.fn();

let req, res, next;

beforeEach(() => {
  req = httpMocks.createRequest();
  res = httpMocks.createResponse();
  next = jest.fn();
});

describe("ProductController.getProducts", () => {
  it("should have a getProducts function", () => {
    expect(typeof ProductController.getProducts).toBe("function");
  });
  it("should call Product.findAndCountAll", async () => {
    const expectedArgs = {
      attributes: ["id", "title", "slug", "sku", "price", "stockLevel"],
      limit: 10,
      offset: 0,
    };
    await ProductController.getProducts(req, res, next);
    expect(models.Product.findAndCountAll).toBeCalledWith(expectedArgs);
  });
  it("should return 200 response code and json", async () => {
    models.Product.findAndCountAll.mockReturnValue({
      rows: allProductsData,
      count: 2,
    });
    await ProductController.getProducts(req, res, next);
    expect(res.statusCode).toBe(200);
    expect(res._getJSONData()).toStrictEqual({
      data: allProductsData,
      page: 1,
      limit: 10,
      totalCount: 2,
    });
  });
  it("should handle errors", async () => {
    const errorMessage = { message: "Something wrong" };
    const rejectedPromise = Promise.reject(errorMessage);
    models.Product.findAndCountAll.mockReturnValue(rejectedPromise);
    await ProductController.getProducts(req, res, next);
    expect(next).toBeCalledWith(errorMessage);
  });
});
