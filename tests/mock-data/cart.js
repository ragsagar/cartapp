const mockCart = {
  id: 10,
  CartItems: [
    {
      id: 11,
      Product: { id: 1, title: "Samsung note 10", stockLevel: 1 },
      price: 100,
      quantity: 2,
      discount: 10,
      sku: "AG100",
    },
    {
      id: 12,
      Product: { id: 2, title: "One plus 8", stockLevel: 10 },
      price: 150,
      quantity: 1,
      discount: 0,
      sku: "AG101",
    },
  ],
  getSubTotal() {
    return 350;
  },
  getTotalDiscount() {
    return 10;
  },
  getShippingCharge() {
    return 0;
  },
  getTotal() {
    return 340;
  },
  getCartData() {
    return mockCartResponse;
  },
  reload() {},
};

const mockCartItem = {
  id: 11,
  ProductId: 3,
  reload() {},
};

const mockProduct = {
  id: 3,
  stockLevel: 10,
};

const mockCartResponse = {
  items: [
    {
      id: 11,
      title: "Samsung note 10",
      productId: 1,
      price: 100,
      quantity: 2,
      stockLevel: 1,
      discount: 10,
      sku: "AG100",
    },
    {
      id: 12,
      title: "One plus 8",
      productId: 2,
      price: 150,
      quantity: 1,
      stockLevel: 10,
      discount: 0,
      sku: "AG101",
    },
  ],
  subTotal: 350,
  shippingCharge: 0,
  discount: 10,
  total: 340,
  id: 10,
};

module.exports = {
  mockCart,
  mockCartItem,
  mockCartResponse,
  mockProduct,
};
