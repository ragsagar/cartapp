const { StatusCodes } = require("http-status-codes");
const request = require("supertest");

const app = require("../../app");
const models = require("../../models");
const newUser = require("../mock-data/new-user.json");

const getCartURL = "/api/v1/carts/mine/";

describe(getCartURL, () => {
  beforeEach(async () => {
    await models.Cart.destroy({ truncate: { cascade: true } });
    await models.Product.destroy({ truncate: { cascade: true } });
    await models.CartItem.destroy({ truncate: { cascade: true } });
    await models.User.destroy({ truncate: { cascade: true } });
    await models.Category.destroy({ truncate: { cascade: true } });
  });
  it("GET should give 401 if there is no token", async () => {
    const response = await request(app).get(getCartURL);
    expect(response.statusCode).toBe(StatusCodes.UNAUTHORIZED);
  });

  it("GET should return 200 with empty json if there are no items in cart.", async () => {
    const userData = Object.assign({}, newUser, { email: "user1@gmail.com" });
    const user = await models.User.create(userData);
    const jwtToken = await user.generateToken();
    const response = await request(app)
      .get(getCartURL)
      .set("Authorization", "Bearer " + jwtToken);
    expect(response.statusCode).toBe(StatusCodes.OK);
    expect(response.body).toStrictEqual({});
  });

  it("GET should give cart response for current user", async () => {
    const userData = Object.assign({}, newUser, { email: "user2@gmail.com" });
    const user = await models.User.create(userData);
    const jwtToken = await user.generateToken();
    const category = await models.Category.create({
      name: "Mobile",
      slug: "mobile",
    });
    const product1 = await models.Product.create({
      title: "Samsung note 10",
      sku: "sg100",
      slug: "samsung-note-10",
      price: 100,
      stockLevel: 10,
      CategoryId: category.id,
    });
    const product2 = await models.Product.create({
      title: "Oneplus 8",
      sku: "sg101",
      slug: "oneplus-9",
      price: 50,
      stockLevel: 5,
      CategoryId: category.id,
    });
    const cart = await models.Cart.create({ UserId: user.id });
    await models.CartItem.create({
      CartId: cart.id,
      ProductId: product1.id,
      sku: product1.sku,
      price: product1.price,
      quantity: 2,
    });
    await models.CartItem.create({
      CartId: cart.id,
      ProductId: product2.id,
      sku: product2.sku,
      price: product2.price,
      quantity: 1,
    });
    const response = await request(app)
      .get(getCartURL)
      .set("Authorization", "Bearer " + jwtToken);
    expect(response.statusCode).toBe(StatusCodes.OK);
    expect(response.body.items.length).toBe(2);
    expect(response.body.subTotal).toBe(250);
    expect(response.body.discount).toBe(0);
    expect(response.body.shippingCharge).toBe(0);
    expect(response.body.total).toBe(250);
  });
});

afterAll(async () => {
  await models.sequelize.close();
});
