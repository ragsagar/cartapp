const request = require("supertest");
const app = require("../../app");
const models = require("../../models");

const endPointURL = "/api/v1/products/";

const createProducts = async () => {
  const category = await models.Category.create({
    name: "Mobile",
    slug: "mobile",
  });
  await models.Product.create({
    id: 1,
    title: "Samsung Note 10",
    sku: "MBS1000",
    price: 2000,
    stockLevel: 10,
    slug: "samsung-note-10",
    CategoryId: category.id,
  });
  await models.Product.create({
    id: 2,
    title: "One plus 8",
    sku: "MBO1001",
    price: 1500,
    stockLevel: 10,
    slug: "one-plus-8",
    CategoryId: category.id,
  });
};

describe(endPointURL, () => {
  beforeEach(async () => {
    await models.Product.destroy({ truncate: { cascade: true } });
    await models.Category.destroy({ truncate: { cascade: true } });
  });
  it("GET " + endPointURL, async () => {
    const expectedData = [
      {
        id: 1,
        title: "Samsung Note 10",
        sku: "MBS1000",
        slug: "samsung-note-10",
        price: "2000",
        stockLevel: 10,
      },
      {
        id: 2,
        title: "One plus 8",
        sku: "MBO1001",
        slug: "one-plus-8",
        price: "1500",
        stockLevel: 10,
      },
    ];
    await createProducts();
    const response = await request(app).get(endPointURL);
    expect(response.statusCode).toBe(200);
    expect(response.body.data).toStrictEqual(expectedData);
  });
  it("should return empty 200 response where there is no data", async () => {
    const response = await request(app).get(endPointURL);
    expect(response.statusCode).toBe(200);
    expect(response.body.data.length).toBe(0);
  });
});

afterAll(async () => {
  await models.sequelize.close();
});
