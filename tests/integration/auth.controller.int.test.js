const request = require("supertest");
const { StatusCodes } = require("http-status-codes");

const app = require("../../app");
const models = require("../../models");
const newUser = require("../mock-data/new-user.json");

const registerURL = "/api/v1/auth/register";
const loginURL = "/api/v1/auth/login/";

describe(registerURL, () => {
  beforeEach(async () => {
    await models.User.destroy({ truncate: { cascade: true } });
  });
  it("POST should create user and return 201", async () => {
    const response = await request(app).post(registerURL).send(newUser);
    expect(response.statusCode).toBe(StatusCodes.CREATED);
    expect(response.body.firstName).toBe("Rust");
    expect(response.body.lastName).toBe("Cohle");
    expect(response.body.email).toBe("rust@gmail.com");
    expect(response.body.id).toBeDefined();
    expect(response.body.password).not.toBeDefined();
  });

  it("POST with invalid input should give validation error", async () => {
    const inputData = Object.assign({}, newUser, {
      email: "rust@sss",
      password: "",
    });
    const response = await request(app).post(registerURL).send(inputData);
    expect(response.statusCode).toBe(StatusCodes.BAD_REQUEST);
    expect(response.body).toStrictEqual({
      errors: [
        { field: "email", message: '"email" must be a valid email' },
        { field: "password", message: '"password" is not allowed to be empty' },
      ],
      message: "Validation error",
    });
  });
});

describe(loginURL, () => {
  beforeEach(async () => {
    await models.User.destroy({ truncate: { cascade: true } });
  });

  it("POST with valid credentials should return 200 and token", async () => {
    await models.User.create(newUser);
    const credentials = {
      email: "rust@gmail.com",
      password: "verystrongpassword",
    };
    const response = await request(app).post(loginURL).send(credentials);
    expect(response.statusCode).toBe(StatusCodes.OK);
    expect(response.body.token).toBeDefined();
  });

  it("POST with non existing user should return 401", async () => {
    const credentials = { email: "someuser@gmail.com", password: "invalid" };
    const response = await request(app).post(loginURL).send(credentials);
    expect(response.statusCode).toBe(StatusCodes.UNAUTHORIZED);
    expect(response.body).toStrictEqual({
      message: "Invalid username or password",
    });
  });

  it("POST with invalid password should return 401", async () => {
    await models.User.create(newUser);
    const credentials = { email: "rust@gmail.com", password: "password" };
    const response = await request(app).post(loginURL).send(credentials);
    expect(response.statusCode).toBe(StatusCodes.UNAUTHORIZED);
    expect(response.body).toStrictEqual({
      message: "Invalid username or password",
    });
  });

  it("POST with invalid input should return 400", async () => {
    const inputData = { email: "rust@" };
    const response = await request(app).post(loginURL).send(inputData);
    expect(response.statusCode).toBe(StatusCodes.BAD_REQUEST);
    expect(response.body).toStrictEqual({
      errors: [
        { field: "email", message: '"email" must be a valid email' },
        { field: "password", message: '"password" is required' },
      ],
      message: "Validation error",
    });
  });
});

afterAll(async () => {
  await models.sequelize.close();
});
