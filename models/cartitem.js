"use strict";
const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  class CartItem extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      CartItem.belongsTo(models.Cart);
      CartItem.belongsTo(models.Product);
    }

    getTotal() {
      return parseFloat(this.price) * this.quantity;
    }

    getDiscount() {
      return parseFloat(this.discount);
    }
  }
  CartItem.init(
    {
      sku: DataTypes.STRING,
      price: {
        type: DataTypes.DECIMAL,
        defaultValue: 0,
        allowNull: false,
      },
      discount: {
        type: DataTypes.DECIMAL,
        defaultValue: 0,
        allowNull: false,
      },
      quantity: {
        type: DataTypes.INTEGER,
        defaultValue: 1,
        allowNull: false,
      },
      CartId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: "Cart",
          key: "id",
        },
      },
      ProductId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: "Product",
          key: "id",
        },
      },
    },
    {
      sequelize,
      modelName: "CartItem",
    }
  );
  return CartItem;
};
