"use strict";
const { Model } = require("sequelize");

const SHIPPING_CHARGE = 0;

module.exports = (sequelize, DataTypes) => {
  class Cart extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Cart.hasMany(models.CartItem);
      Cart.belongsTo(models.User);
    }

    getSubTotal() {
      return this.CartItems.reduce((acc, item) => acc + item.getTotal(), 0);
    }

    getTotalDiscount() {
      return this.CartItems.reduce((acc, item) => acc + item.getDiscount(), 0);
    }

    getShippingCharge() {
      return SHIPPING_CHARGE;
    }

    getTotal() {
      return (
        this.getSubTotal() + this.getShippingCharge() - this.getTotalDiscount()
      );
    }

    getCartData() {
      return {
        id: this.id,
        subTotal: this.getSubTotal(),
        discount: this.getTotalDiscount(),
        shippingCharge: this.getShippingCharge(),
        total: this.getTotal(),
        items: this.CartItems.map((item) => ({
          id: item.id,
          title: item.Product.title,
          price: item.price,
          quantity: item.quantity,
          stockLevel: item.Product.stockLevel,
          productId: item.Product.id,
          sku: item.sku,
          discount: item.discount,
        })),
      };
    }
  }
  Cart.init(
    {
      status: {
        type: DataTypes.ENUM,
        values: ["new", "complete"],
        defaultValue: "new",
      },
      UserId: {
        type: DataTypes.INTEGER,
        references: {
          model: "User",
          key: "id",
        },
      },
    },
    {
      sequelize,
      modelName: "Cart",
    }
  );
  return Cart;
};
