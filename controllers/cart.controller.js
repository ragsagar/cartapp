const { StatusCodes } = require("http-status-codes");

const models = require("../models");
const {
  catchAsyncError,
  constructValidationErrorMessage,
} = require("../utils/errorHandlers");

// API to fetch all the items in the cart. If there is no active cart present for current user,
// empty cart response will be returned.
const getCart = async (req, res) => {
  const cart = await models.Cart.findOne({
    where: { UserId: req.user.id, status: "new" },
    include: { model: models.CartItem, include: { model: models.Product } },
  });
  let cartResponse;
  if (!cart) {
    cartResponse = {};
  } else {
    cartResponse = cart.getCartData();
  }
  res.status(StatusCodes.OK).json(cartResponse);
};

// If there is no active cart for current user, it will be created and then new item will be added
// or existing item's quantity will be incremented.
const addItemToCart = async (req, res) => {
  // Get product and check if it exists.
  const product = await models.Product.findByPk(req.body.productId);
  if (!product) {
    const errorMessage = constructValidationErrorMessage(
      "productId",
      "Invalid productId"
    );
    return res.status(StatusCodes.BAD_REQUEST).json(errorMessage);
  }
  // Check if there are enough items in stock.
  if (product.stockLevel < req.body.quantity) {
    const errorMessage = constructValidationErrorMessage(
      "quantity",
      "Not enough items in stock"
    );
    return res.status(StatusCodes.BAD_REQUEST).json(errorMessage);
  }

  // Find the cart or create one
  const [cart] = await models.Cart.findOrCreate({
    where: { UserId: req.user.id, status: "new" },
  });

  // Check if the item exists in cart
  // Increase the quantity if it exists, otherwise create new.
  const [cartItem, cartItemCreated] = await models.CartItem.findOrCreate({
    where: { CartId: cart.id, ProductId: req.body.productId },
    defaults: {
      quantity: req.body.quantity,
      price: product.price,
      discount: product.discount,
      sku: product.sku,
    },
  });
  // Atomically increment the quantity if the item is already there in db.
  if (!cartItemCreated) {
    // We need to check stock level again here, since it can go above the stock level when we update.
    if (product.stockLevel < cartItem.quantity + req.body.quantity) {
      const errorMessage = constructValidationErrorMessage(
        "quantity",
        "Not enough items in stock"
      );
      return res.status(StatusCodes.BAD_REQUEST).json(errorMessage);
    }
    await cartItem.increment("quantity", { by: req.body.quantity });
  }
  await cartItem.reload();
  res.status(StatusCodes.CREATED).json(cartItem);
};

// API to update the quanity of an existing item in cart.
const updateCartItem = async (req, res) => {
  const cartItem = await models.CartItem.findOne({
    where: {
      id: req.params.id,
      "$Cart.UserId$": req.user.id,
      "$Cart.status$": "new",
    },
    include: [
      { model: models.Cart },
      { model: models.Product, required: true },
    ],
  });
  if (!cartItem) {
    return res
      .status(StatusCodes.NOT_FOUND)
      .json({ message: "Cart item not found" });
  }
  // Check if there are enough items in stock.
  if (cartItem.Product.stockLevel < req.body.quantity) {
    const errorMessage = constructValidationErrorMessage(
      "quantity",
      "Not enough items in stock"
    );
    return res.status(StatusCodes.BAD_REQUEST).json(errorMessage);
  }
  await cartItem.update(req.body);
  // mysql doesn't return the data on update.
  await cartItem.reload();
  res.status(StatusCodes.OK).json(cartItem.toJSON());
};

// API to delete existing item in the cart.
const deleteItem = async (req, res) => {
  // Sequelize doesn't support join queries in destroy.
  // So we have to fetch and delete or use raw query to do it in one db call.
  const cartItem = await models.CartItem.findOne({
    where: {
      id: req.params.id,
      "$Cart.UserId$": req.user.id,
      "$Cart.status$": "new",
    },
    include: { model: models.Cart },
  });
  if (!cartItem) {
    res.status(StatusCodes.NOT_FOUND).json({ message: "Cart item not found" });
  } else {
    await cartItem.destroy();
    res.status(StatusCodes.NO_CONTENT).end();
  }
};

module.exports = {
  getCart: catchAsyncError(getCart),
  addItemToCart: catchAsyncError(addItemToCart),
  deleteItem: catchAsyncError(deleteItem),
  updateCartItem: catchAsyncError(updateCartItem),
};
