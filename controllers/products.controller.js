const { StatusCodes } = require("http-status-codes");
const { Product, Category } = require("../models");
const { catchAsyncError } = require("../utils/errorHandlers");
const { getPageLimitAndOffset } = require("../utils/pagination");

// API to get all products
const getProducts = async (req, res) => {
  const [page, limit, offset] = getPageLimitAndOffset(
    req.query.page,
    req.query.limit
  );
  attributes = ["id", "title", "slug", "sku", "price", "stockLevel"];
  const options = {
    attributes: attributes,
    limit: limit,
    offset: offset,
  };
  if (req.query.category) {
    options.include = { model: Category };
    options.where = { "$Category.slug$": req.query.category };
  }
  const result = await Product.findAndCountAll(options);
  res.status(StatusCodes.OK).json({
    data: result.rows,
    totalCount: result.count,
    page: page,
    limit: limit,
  });
};

module.exports = {
  getProducts: catchAsyncError(getProducts),
};
