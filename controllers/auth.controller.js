const { StatusCodes } = require("http-status-codes");

const { User } = require("../models");
const { catchAsyncError } = require("../utils/errorHandlers");

const registerResponseAttrs = ["id", "firstName", "lastName", "email"];

const registerUser = async (req, res) => {
    const user = await User.create(req.body);
    const userData = registerResponseAttrs.reduce(
      (acc, attribute) => ({ ...acc, [attribute]: user[attribute] }),
      {}
    );
    res.status(StatusCodes.CREATED).json(userData);
};

const loginUser = async (req, res) => {
  const errorMessage = { message: "Invalid username or password" };
  const user = await User.findOne({ where: { email: req.body.email } });
  if (!user) {
    res.status(StatusCodes.UNAUTHORIZED).json(errorMessage);
  } else {
    const isPasswordMatching = await user.comparePassword(req.body.password);
    if (!isPasswordMatching) {
      res.status(StatusCodes.UNAUTHORIZED).json(errorMessage);
    } else {
      res.status(StatusCodes.OK).json({ token: user.generateToken() });
    }
  }
};

module.exports = {
  registerUser: catchAsyncError(registerUser),
  loginUser: catchAsyncError(loginUser),
};
